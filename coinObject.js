

const coin = {
    state: 0,
    flip: function() {
        this.state = Math.floor(Math.random() * 2);
        
    },
    toString: function() {
        if(this.state == 0){
            return "Tails";
        }else    {
            return "Heads";
        } 
    },
    toHTML: function() {
        const image = document.createElement('img');
        image.style.height='90px'
        if(this.state == 0){
            image.src = "src/Heads.png";
        }else {
            image.src = "src/Tails.png";
        }
        return image;
    }
};
function display20Flips() {
    const results = [];
    for(let i = 0; i < 20; i ++){
        coin.flip();
        let div = document.createElement("div")
        div.innerHTML = (coin.toString());
        document.body.appendChild(div);
        results.push(coin.state);
    }
    return results;
}
display20Flips()

function display20Images() {
    const results = [];
    for(let i = 0; i < 20; i ++){
        coin.flip();
        let image = coin.toHTML();
        document.body.appendChild(image);
        results.push(coin.state);

    }
    return results;
}
display20Images()